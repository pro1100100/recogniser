# (Face) Recogniser

This tool is able to detect and identify objects of interest (currently only faces) in a video or image.
The module consists of following submodules:
1. [MTCNN](https://kpzhang93.github.io/MTCNN_face_detection_alignment/index.html) for face detection
2. [FaceNet](https://arxiv.org/abs/1503.03832) to reduce faces to Embedding-vectors
3. A custom classifier to map an Embedding vector to a particular person.
4. *(In plan)* A post processor to improve final recognition results.

## Getting Started

### Install dependencies:
``` 
pip install -r requrements.txt
conda install av -c conda-forge
```

*Note: There also ways to install av in a different way, but with conda it is straight forward.* 


### How to run

Execute directly: 
```
python  run.py path_to_video/video.mp4 \
        path_to_facenet_model/facenet_model.pb \
        path_to_classifer_keras_model/classifier_model.h5 \
        path_to_pickle_map_class_id_to_class_name \
        --dnn_classifier_conf_threshold <float> \
        --annoy_index_file_path <Path to Annoy index file> \
        --annoy_id_name_map_path <Path to annoy feature vector id - name map>
        
```

For more details run `python  run.py -h`

## Running the tests

All submodules contain small builtin tests which you can execute by running the respective module directly:

```
    Detector:   python detector.py path_to_img/img.jpg
    Embedder:   python embedder.py path_to_img/img.jpg path_to_facenet_model/facenet_model.pb
    DNNClassifier: -
    AnnoyKNNClassifier: -
    Recogniser: python path_to_video_or_img/file.ext \
                path_to_facenet_model/facenet_model.pb \
                path_to_classifer_keras_model/classifier_model.h5 \
                path_to_pickle_map_class_id_to_class_name
```


## Make installer
From project main directory (adopt `setup.py` if needed) and run:
```bash
python setup.py sdist bdist_wheel
```


## Acknowledgments

Many thanks to authors and contributors of following deep learning repositories:
* [Facenet- Face Recognition using Tensorflow](https://github.com/davidsandberg/facenet)
* [MTCNN](https://github.com/ipazc/mtcnn)
* [Annoy](https://github.com/spotify/annoy)

also many thanks to the authors and contributors of other libraries used in this project.

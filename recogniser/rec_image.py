from recogniser.recogniser import Recogniser
from recogniser.detector import MTCNNDetector
from recogniser.embedder import Embedder
from recogniser.classifier import DNNClassifier, AnnoyKNNClassifier
import imghdr
import cv2
from math import ceil
from PIL import Image
import requests
from io import BytesIO
import os
import numpy as np
import json_tricks as jt



class ImageRecogniser(Recogniser):

    def __init__(self, file_path, detector, embedder, dnn_classifier, annoy_classifier):
        """
        Constructor.

        :param file_path: Path to image file.
        :param detector: Instance of Detector class.
        :param embedder: Instance of Embedder class.
        :param dnn_classifier: Instance of DNNClassifier class.
        :param annoy_classifier: Instance of AnnoyKNNClassifier class.
        """
        super(ImageRecogniser, self).__init__(detector, embedder, dnn_classifier, annoy_classifier)
        self.__file_path = file_path
        assert(imghdr.what(self.__file_path) in ['png', 'jpeg', 'jpg'])
        img = cv2.imread(self.__file_path)[:,:,::-1]
        if len(img.shape)==2:
            img = np.dstack((img, img, img))

        self.__image = img

    def process(self):
        """
        Main processing function returns detections, classes, names and genders as result dict for provided image.
        :return: detections, classes, names, genders
        """

        detections, _, embs, confidences, classes, names, genders = self.process_images([self.__image])

        names, classes, confidences = self.cluster_ebmeddings(embs, names, classes, confidences)

        result = Recogniser.return_results(detections=detections, classes=classes, names=names, genders=genders, confidences=confidences)

        return result

    @staticmethod
    def test_single_image(args):
        """
        Test method for a single image. See the bottom of the file for more details.

        :param args:
        """
        detector = MTCNNDetector()
        embedder = Embedder(model_path=args.emb_model_path)
        dnn_classifier = DNNClassifier(classifier_path=args.dnn_classifier_model_path,
                                       class_map_path = args.dnn_classifier_id_name_map_path,
                                       threshold=args.dnn_classifier_conf_threshold)
        annoy_classifier = AnnoyKNNClassifier(annoy_index_file_path=args.annoy_index_file_path,
                                              id_name_map_path=args.annoy_id_name_map_path)

        recogniser = ImageRecogniser(file_path=args.input_file_path,
                                     detector=detector,
                                     embedder=embedder,
                                     dnn_classifier=dnn_classifier,
                                     annoy_classifier=annoy_classifier)
        detections, classes, names, gender, confidences = recogniser.process()

        display_img = recogniser.draw_annotations(recogniser.__image, detections[0], classes=classes[0], names=names[0],
                                                  confidences=confidences[0])
        display_img = cv2.cvtColor(display_img, cv2.COLOR_BGR2RGB)
        cv2.imshow("display_img", display_img)

        while True:
            key = cv2.waitKey(1000)
            if key == 27:  # 27 -> Esc
                break

class UrlImageRecogniser(Recogniser):

    def __init__(self, img_urls, detector, embedder, dnn_classifier, annoy_classifier, batch_size = 10):
        """
        Constructor.

        :param img_urls: List of image url's.
        :param detector: Instance of Detector class.
        :param embedder: Instance of Embedder class.
        :param dnn_classifier: Instance of DNNClassifier class.
        :param annoy_classifier: Instance of AnnoyKNNClassifier class.
        :param batch_size:
        """

        super(UrlImageRecogniser, self).__init__(detector, embedder, dnn_classifier, annoy_classifier)
        self.__img_urls = img_urls
        for url in self.__img_urls:
            assert(os.path.splitext(url)[-1] in ['.png', '.jpeg', '.jpg'])

        self.__batch_size = batch_size

    def __get_images(self, url_b):
        images = []
        for url in url_b:
            response = requests.get(url)
            img = Image.open(BytesIO(response.content))
            np_image = np.array(img)
            if len(np_image.shape) == 2:
                np_image=np.dstack((np_image, np_image, np_image))
            images.append(np_image)

        return images

    def process(self):
        """
        Main processing function returns detections, classes, names and genders as result dict for provided image.
        :return: detections, classes, names, genders
        """
        detections, embs, confidences, classes, names, genders = [], [], [], [], [], []
        num_batches = ceil(len(self.__img_urls)/self.__batch_size)
        for i in range(num_batches):
            b_start = i*self.__batch_size
            b_end   = (i+1)*self.__batch_size
            url_b = self.__img_urls[b_start:b_end]
            images  = self.__get_images(url_b)

            detections_b, _, embs_b, confidence_b, classes_b, names_b, gender_b = self.process_images(images)
            detections = detections + detections_b
            #crops = crops + crops_b
            confidences = confidences + confidence_b
            embs = embs + embs_b
            classes = classes + classes_b
            names = names + names_b
            genders = genders + gender_b

        names, classes, confidences = self.cluster_ebmeddings(embs, names, classes, confidences)

        result = Recogniser.return_results(detections=detections, classes=classes, names=names, genders=genders, confidences=confidences)

        return result


    @staticmethod
    def test_on_images(args):
        """
        Test method for a single image. See the bottom of the file for more details.

        :param args:
        """
        detector = MTCNNDetector()
        embedder = Embedder(model_path=args.emb_model_path)
        dnn_classifier = DNNClassifier(classifier_path=args.dnn_classifier_model_path,
                                       class_map_path = args.dnn_classifier_id_name_map_path,
                                       threshold=args.dnn_classifier_conf_threshold)
        annoy_classifier = AnnoyKNNClassifier(annoy_index_file_path=args.annoy_index_file_path,
                                              id_name_map_path=args.annoy_id_name_map_path)

        with open(args.input_file_path, "r", encoding="utf-8") as urls_file:
            text = urls_file.read()
            urls = text.split("\n")

        recogniser = UrlImageRecogniser(urls,
                                        detector=detector,
                                        embedder=embedder,
                                        dnn_classifier =dnn_classifier,
                                        annoy_classifier=annoy_classifier,
                                        batch_size=1)
        json_res = recogniser.process()

        detections = json_res['detections']
        classes = json_res['classes']
        names = json_res['names']
        genders = json_res['genders']
        confidences = json_res['confidences']

        imgaes = recogniser.__get_images(urls)
        for i in range(len(imgaes)):

            display_img = recogniser.draw_annotations(imgaes[i], detections[i], classes=classes[i], names=names[i],
                                                      genders=genders[i], confidences=confidences[i])
            display_img = cv2.cvtColor(display_img, cv2.COLOR_BGR2RGB)
            cv2.imshow("display_img", display_img)

            while True:
                key = cv2.waitKey(1000)
                if key == 27:  # 27 -> Esc
                    exit(0)
                if key == 32:  # Space
                    break



from mtcnn.mtcnn import MTCNN
import argparse
import numpy as np
import cv2

class Detector():
    def __init__(self, confidence_threshold):
        self.confidence_threshold = confidence_threshold

    def detect(self, images):
        raise NotImplementedError

    def __safe_bbox(self, detections, image_shape):
        """
        Makes sure all the bounding boxex do not exceed image borders.

        :param detections: Detection objects from MTCNN.
        :param image_shape: Shape of the image where detections were made.
        :return: save_detections: Detections where each bounding box is guaranteed to be inside the image.
        """
        save_detections = []
        i_h, i_w  = image_shape[0:2]
        for entry in detections:
            if entry['confidence'] > self.confidence_threshold:
                x,y,w,h = entry['box']
                x = int(np.max([0,x]))
                y = int(np.max([0,y]))
                w = int(i_w-x if (x+w)>i_w else w)
                h = int(i_h - y if (y + h) > i_h else h)
                entry['box'] = [x,y,w,h]
                save_detections.append(entry)
        return save_detections

class MTCNNDetector(Detector):
    """
    MTCNN based face detector class.
    """
    def __init__(self, confidence_threshold=0.9):
        super(MTCNNDetector, self).__init__(confidence_threshold)
        self.detector = MTCNN()


    def detect(self, images):
        """
        Main Detector method. Detect faces in given images.

        :param images: Images where to detect.
        :return: detections: List of detections for every image.
        """

        detections = []

        for image in images:
            faces = self.detector.detect_faces(image)
            faces = self._Detector__safe_bbox(faces, image.shape)
            detections.append(faces)

        return detections

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('image_path', type=str, help='Test image path.')
    args = parser.parse_args()

    image = cv2.imread(args.image_path)
    detector = MTCNNDetector()

    result = detector.detect([image, image])

    print(result)
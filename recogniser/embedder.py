import tensorflow as tf
import numpy as np
import cv2
import argparse
from tensorflow.python.platform import gfile

class Embedder:
    """
    Process images of objects (faces) to respective embedding vectors using FaceNet.
    """
    def __init__(self, model_path, input_size = (160,160), logdir = None):
        """
        Constructor.

        :param model_path: Path to the facenet model - tensorflow .pb file.
        :param input_size: Size the iamges should have before forward pass to the network.
        :param logdir: Optional parameter, if it is set, saves the graph in tensorboard format at given location.
        """

        with gfile.FastGFile(model_path, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())

        self.sess = tf.Session()
        self.sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')

        if logdir:
            writer = tf.summary.FileWriter(logdir)
            writer.add_graph(self.sess.graph)

        self.input_size = input_size

        # Get input and output tensors
        self.images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
        self.embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
        self.phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
        self.embedding_size = self.embeddings.get_shape()[1]

    def __prewhiten(self, x):
        """
        Normalize an image.

        :param x: Image.
        :return: Normalized image.
        """
        mean = np.mean(x)
        std = np.std(x)
        std_adj = np.maximum(std, 1.0/np.sqrt(x.size))
        y = np.multiply(np.subtract(x, mean), 1/std_adj)
        return y

    def to_embeddings(self, images, do_prewhiten=True):
        """
        Forward pass the images through the Facenet model.

        :param images: Batch of images (croped faces) to apply Facenet to.
        :param do_prewhiten: If true, normalise rhe image before forward pass.
        :return:
        """
        images_resized = []
        for img in images:
            if do_prewhiten:
                img = self.__prewhiten(img)
            images_resized.append(cv2.resize(img, dsize=self.input_size))

        emb_array = np.zeros((len(images_resized), self.embedding_size))
        feed_dict = {self.images_placeholder: images_resized, self.phase_train_placeholder: False}
        emb_array = self.sess.run(self.embeddings, feed_dict=feed_dict)

        return emb_array

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('image_path', type=str, help='Test image path.')
    parser.add_argument('model_path', type=str, help='Path to facenet model.')
    args = parser.parse_args()

    img = cv2.imread(args.image_path)
    images = [img, img, img]

    emb = Embedder(args.model_path)
    embeddings = emb.to_embeddings(images)

    print(embeddings)
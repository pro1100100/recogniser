import cv2
from scipy.spatial import cKDTree
import numpy as np
import uuid


class Recogniser():

    @staticmethod
    def return_results(**kwargs):
        return kwargs

    def __init__(self, detector, embedder, dnn_classifier, annoy_classifier, unknown_tag="_Unknown"):
        """
        Constructor.

        :param annoy_classifier:
        :param detector: Instance of Detector class.
        :param embedder: Instance of Embedder class.
        :param dnn_classifier: Instance of DNNClassifier class.
        """
        self.detector = detector
        self.embedder = embedder
        self.dnn_classifier = dnn_classifier
        self.annoy_classifier = annoy_classifier
        self.unknown_tag = unknown_tag

        self.__name_color_map = {}
        self.__colors = {
            'white': (255, 255, 255)
        }

    def __crop_detections(self, images, image_detections):
        """
        Crop the detected bbox from the original image for all given images and respective detections.

        :param images: Set of images.
        :param image_detections: Detections for every image.
        :return: List of images cropped  from the original ones.
        """
        all_crops = []
        for i, detection_result in enumerate(image_detections):
            img = images[i]
            single_image_crop_list = []
            for d in detection_result:
                x, y, w, h = d['box']
                sub_img = img[y:y + h, x:x + w]
                single_image_crop_list.append(sub_img)
            all_crops.append(single_image_crop_list)
        return all_crops

    def __flat2nested(self, nested_example_list, flat_list):
        """
        Returns an flat array to the proper nested form, using another array which is already in proper from.

        :param nested_example_list: Any list already in required form.
        :param flat_list: List which needs to look like nested_example_list.
        :return: new_nested_list, is flat_list in form/shape of nested_example_list.
        """
        new_nested_list = []
        start = 0
        if type(flat_list) == np.ndarray:
            flat_list = flat_list.tolist()

        for i in range(len(nested_example_list)):
            sub_list = nested_example_list[i]
            sub_list_len = len(sub_list)
            next_start = start + sub_list_len
            new_nested_list.append(flat_list[start:next_start])
            start = next_start

        if type(new_nested_list) == np.ndarray:
            new_nested_list = new_nested_list.tolist()

        return new_nested_list

    def cluster_ebmeddings(self, embeddings, names, classes, confidences, dist_th=0.5):
        flat_embs = [entry for sublist in embeddings for entry in sublist]
        flat_names = np.hstack(names)
        flat_classes = np.hstack(classes)
        flat_confidences = np.hstack(confidences)

        if flat_embs == []:
            return names, classes, confidences

        tree = cKDTree(data=flat_embs)

        for i in range(len(flat_names)):
            near_emb_indexes = tree.query_ball_point(flat_embs[i], dist_th)

            candidate_names = flat_names[near_emb_indexes]
            unique_names, name_counts = np.unique(candidate_names, return_counts=True)

            winner_result_name_id = np.argmax(name_counts)
            winner_name = unique_names[winner_result_name_id]
            winner_class_id = self.dnn_classifier.inverted_class_map.get(winner_name, -1)

            mean_confidence = np.mean(flat_confidences[near_emb_indexes][candidate_names == winner_name])

            if (winner_name == self.unknown_tag):
                winner_name = str(uuid.uuid4())[:8]

            flat_names[near_emb_indexes] = winner_name
            flat_classes[near_emb_indexes] = winner_class_id
            flat_confidences[near_emb_indexes] = mean_confidence

        new_names = self.__flat2nested(names, flat_names)
        new_classses = self.__flat2nested(classes, flat_classes)
        new_confidences = self.__flat2nested(confidences, flat_confidences)
        return new_names, new_classses, new_confidences

    def draw_annotations(self, img, detections, classes=None, names=None, genders=None, confidences=None):
        """
        Draw bounding boxes on image.

        :param img: Image where to draw.
        :param detections: Objects containing bounding boxes to draw.
        :param classes: Class id's for corresponding bounding boxes.
        :param names: Class names for corresponding bounding boxes.
        :return: display_img: Original image with Annotated bounding boxes on it.
        """
        display_img = img.copy()
        for i, d in enumerate(detections):
            x, y, w, h = d['box']
            cv2.rectangle(display_img, (x, y), (x + w, y + h), (255, 255, 255))

            if classes is not None:
                cv2.putText(display_img, f"Class ID: {str(int(classes[i]))}", (x, y + h + 15), cv2.FONT_HERSHEY_PLAIN,
                            1, self.__colors['white'])

            if confidences is not None:
                cv2.putText(display_img, f"Conf.: {str(round(confidences[i], 2))}", (x, y + h + 30),
                            cv2.FONT_HERSHEY_PLAIN,
                            1, self.__colors['white'])

            if names is not None:
                if names[i] in self.__name_color_map:
                    name_color = self.__name_color_map[names[i]]
                else:
                    name_color = np.random.randint(0, 255, 3).tolist()
                    self.__name_color_map[names[i]] = name_color
                cv2.putText(display_img, "Name: ", (x, y + h + 45), cv2.FONT_HERSHEY_PLAIN, 1, self.__colors['white'])
                cv2.putText(display_img, names[i], (x + 55, y + h + 45), cv2.FONT_HERSHEY_PLAIN, 1, name_color)

            if genders is not None:
                if (genders[i] == 1):
                    gender = 'W'
                    color = (255, 20, 147)  # pink

                elif (genders[i] == 0):
                    gender = 'M'
                    color = (0, 255, 0)
                cv2.putText(display_img, "Gender: ", (x, y + h + 60), cv2.FONT_HERSHEY_PLAIN, 1, self.__colors['white'])
                cv2.putText(display_img, gender, (x + 71, y + h + 60), cv2.FONT_HERSHEY_PLAIN, 1, color)

        # cv2.putText(display_img, "Det. Conf: {:.02f}".format(d['confidence']), (x,y-15), cv2.FONT_HERSHEY_PLAIN, 1, (255,255,255))

        return display_img

    def merge_dnn_and_annoy_results(self, dnn_names, dnn_classes, annoy_candidates):

        for i, (name, cl, candidates) in enumerate(zip(dnn_names, dnn_classes, annoy_candidates)):
            if (cl != -1) and (name not in candidates):
                dnn_names[i] = self.unknown_tag
                dnn_classes[i] = -1
        return dnn_names, dnn_classes

    def process(self):
        raise NotImplementedError

    def process_images(self, images):
        """
        Applies the recognition to any given list of images.

        :param images: Images to apply the recognition to.
        :return: Information created during the recognition for each image.
                 detections - Face detections.
                 crops - croped faces.
                 nested_embs - Facenet embeddings.
                 nested_classes - Classes of the embeddings.
                 nested_names - Names of the classes of the embeddings.
        """
        print("Run detector...")
        detections = self.detector.detect(images)
        print("Detections done!")

        if any(detections):

            print("croping detections...")
            crops = self.__crop_detections(images, detections)
            flat_crops = [item for sublist in crops for item in sublist]
            print("croping done!")

            print("calculating embeddings...")
            embs = self.embedder.to_embeddings(flat_crops)
            nested_embs = self.__flat2nested(detections, embs)
            print("embeddings done!")

            print("dnn classification...")
            confidences, classes, names, genders = self.dnn_classifier.classify(embs)
            nested_genders = self.__flat2nested(detections, genders)
            nested_confidences = self.__flat2nested(detections, confidences)
            print("dnn classification done!")

            print("knn classification...")
            candidates = self.annoy_classifier.get_best_candidates(embs)
            names, classes = self.merge_dnn_and_annoy_results(names, classes, candidates)

            nested_classes = self.__flat2nested(detections, classes)
            nested_names = self.__flat2nested(detections, names)
            print("knn classification done!")

            return detections, crops, nested_embs, nested_confidences, nested_classes, nested_names, nested_genders

        else:
            return detections, detections, detections, detections, detections, detections, detections

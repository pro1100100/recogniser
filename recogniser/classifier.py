from keras.models import load_model
import numpy as np
import pickle as p
import os
import json
from annoy import AnnoyIndex


class DNNClassifier:
    """
    Classifies embeddings to a coresponding class.
    """

    def __init__(self, classifier_path, class_map_path, threshold=0.60):
        """
        DNNClassifier constructor.

        :param classifier_path: Path to keras classifier model (usually .h5 file)
        :param class_map_path: Path to a dictionary containing class id and class name pairs (json or numpy).
        :param threshold: Threshold for the classifier to decide if person is known.
        """
        self.threshold = threshold
        self.classifier_path = classifier_path
        self.classifier = load_model(self.classifier_path)
        self.class_map = self.__load_map(class_map_path)
        class_map_len = len(self.class_map)
        class_map_eq_net_output_size = ((class_map_len == self.classifier.layers[-1].output_shape[1]) or \
                                        (class_map_len == self.classifier.layers[-2].output_shape[1]))
        assert (class_map_eq_net_output_size)
        self.class_map[-1] = "_Unknown"
        self.inverted_class_map = dict(map(reversed, self.class_map.items()))

    def __load_map(self, path):
        """
        Loads a map id -> name into classifer.

        :param path: file path to .json or .npy with the map soring class id-name pairs.
        :return: Dictionary containing class id-name pairs.
        """
        class_map_format = os.path.splitext(path)[-1]
        with open(path, 'rb') as f:
            if class_map_format == ".json":
                class_map = json.load(f)
                class_map = {int(k): v for k, v in class_map.items()}
            else:
                class_map = p.load(f)
        return class_map

    def classify(self, embeddings):
        """
        Main class method which maps an array of embedding vectors to respective clases
        Each embedding vector, with highest probability lower then classifier thrashold, becomes -1 .

        :param embeddings:
        :return: predictions, class_names - class id and class name pairs.
        """
        (p_probabilities, g_probabilities) = self.classifier.predict(embeddings)
        max_prob = np.max(p_probabilities, axis=1)
        predictions = np.argmax(p_probabilities, axis=1)
        predictions[max_prob < self.threshold] = -1
        predictions = predictions.tolist()

        class_names = [self.class_map.get(cl) for cl in predictions]

        genders = np.round(g_probabilities.ravel()).astype(np.uint8).tolist()

        return max_prob, predictions, class_names, genders

    def get_map(self):
        return self.class_map


class AnnoyKNNClassifier():
    """
    KNN classifier to find nearest vectors for an embedding.
    """
    def __init__(self, annoy_index_file_path, id_name_map_path,
                 feature_vector_len=512, n_points=50, candidat_count_th=5, prefault=False):
        self.annoy_index_file_path = annoy_index_file_path
        self.annoy_index = AnnoyIndex(feature_vector_len, metric='euclidean')
        self.annoy_index.load(self.annoy_index_file_path, prefault)
        self.id_name_map_path = id_name_map_path
        self.n_points = n_points
        self.candidat_count_th = candidat_count_th
        with open(self.id_name_map_path, "rb") as id_name_map_file:
            self.id_name_map = p.load(id_name_map_file)

    def get_best_candidates(self, feature_vectors):
        proposed_candidates = []

        for fv in feature_vectors:
            indicies, distances = self.annoy_index.get_nns_by_vector(fv,
                                                                     self.n_points,
                                                                     include_distances=True)
            all_candidates = [self.id_name_map[i] for i in indicies]
            u_names, names_count = np.unique(all_candidates, return_counts=True)
            feature_vector_proposed_candidates = u_names[names_count >= self.candidat_count_th]
            proposed_candidates.append(feature_vector_proposed_candidates)

        return proposed_candidates

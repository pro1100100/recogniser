import numpy as np


class Postprocessor():
    pass

class IoUPostprocessor(Postprocessor):

    def __init__(self, frame_window_size=10, stride=1, min_detection_frequency=0.5, iou_th = 0.8):
        super(IoUPostprocessor, self).__init__()
        self.frame_window_size = frame_window_size
        self.stride = stride
        self.min_detection_frequency = min_detection_frequency
        self.iou_th = iou_th

    def __IoU(self, d1, d2):
        x1, y1, w1, h1 = d1['box']
        x12 = x1 + w1
        y12 = y1 + h1
        x2, y2, w2, h2 = d2['box']
        x22 = x2 + w2
        y22 = y2 + h2

        xA = max(x1, x2)
        yA = max(y1, y2)
        xB = min(x12, x22)
        yB = min(y12, y22)

        A1 = (w1*h1)
        A2 = (w2*h2)

        intersection = max(xB - xA + 1,0) * max(yB - yA + 1, 0)
        union = (A1 + A2 - intersection)
        union = union if (union > 0) else float('inf')

        iou = intersection / union
        return iou

    #TODO This algorithm can maybe be improved.
    def process(self, frames, detections, class_ids, class_names, class_map):

        for i in range(self.frame_window_size, len(frames), self.stride):
            for di1, d1 in enumerate(detections[i]):
                d1_classes = [class_ids[i][di1]]
                d1_detections = 0

                for j in range(i-self.frame_window_size, i):
                    for di2, d2 in enumerate(detections[j]):
                        iou = self.__IoU(d1, d2)
                        if iou >= self.iou_th:
                            #Save class and increase amount of detection
                            d1_detections += 1
                            d1_classes.append(class_ids[j][di2])
                        else:
                            pass
                #Evaluate if detection is true(frequent) and most likely class.
                if (d1_detections/self.frame_window_size) < self.min_detection_frequency:
                    detections[i].pop(di1)
                    class_ids[i] = np.delete(class_ids[i], di1)
                    class_names[i] = np.delete(class_names[i], di1).tolist()
                else:
                    (values, counts) = np.unique(d1_classes, return_counts=True)
                    c = values[np.argmax(counts)]
                    class_ids[i][di1] = c
                    class_names[i][di1] = class_map[c]

        return detections, class_ids, class_names


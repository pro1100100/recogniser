from recogniser.recogniser import Recogniser
from recogniser.detector import MTCNNDetector
from recogniser.embedder import Embedder
from recogniser.classifier import DNNClassifier, AnnoyKNNClassifier
import time
import os
import av
import numpy as np
import json_tricks as jt
from subprocess import Popen, PIPE, STDOUT, SubprocessError

class VideoRecogniser(Recogniser):
    """
    Main module, containing other submodules, applying them step by step to identify persons on iamges.

    """
    def __init__(self, file_path, detector, embedder, dnn_classifier, annoy_classifier, postprocessor=None,
                 start_frame=0, end_frame=None, batch_split_strategy='uniform',
                 batch_size=250, scene_change_frame_nmumbers = [],
                 save_output=False, output_path=None,
                 ffmpge_path=None, ffmpeg_scene_change_threshold=0.2):
        """

        :param file_path: Path to image file.
        :param detector: Instance of Detector class.
        :param embedder: Instance of Embedder class.
        :param dnn_classifier: Instance of :class:`DNNClassifier` class.
        :param annoy_classifier: Instance of :class:`AnnoyKNNClassifier` class.
        :param postprocessor: Instance of Postprocessor class.
        :param start_frame: Frame where to start to apply the recognition.
        :param end_frame: Frame where to stop to apply the recognition.
        :param batch_split_strategy: default: uniform, oprions -> ("uniform", "ffmpeg", "custom")
        :param batch_size: Number of frames to proceeed at once (used if strategy is "uniform").
        :param scene_change_frame_nmumbers: Frame numbers where scene change appears (strategy "custom")
        :param save_output: Set True to store output video with results.
        :param output_path: Path where to save output file, default is out_timestamp_input-file-name.
        :param ffmpge_path: Only if strategy is "ffmpeg".
        :param ffmpeg_scene_change_threshold: Only if strategy is "ffmpeg", Suitable values are 0.1-0.4.

        """


        super(VideoRecogniser, self).__init__(detector, embedder, dnn_classifier, annoy_classifier)
        self.__file_path = file_path
        assert(os.path.splitext(self.__file_path)[-1] in ['.mp4', '.avi', '.mxf']) # TODO Add more extensions.
        self.postprocessor = postprocessor

        self.__batch_split_strategy = batch_split_strategy
        self.__batch_size           = batch_size

        self.__ffmpeg_path          = ffmpge_path
        if self.__ffmpeg_path is not None:
            assert(os.path.isfile(self.__ffmpeg_path) )
            self.__ffmpeg_timeout = 120
            self.__ffmpeg_frame_dist_threshold = 12
            self.__ffmpeg_scene_change_threshold = ffmpeg_scene_change_threshold

            self.__ffmpeg_cmd = "{} -i {} -hide_banner  -filter:v \"select='gt(scene,0.2)',showinfo\"  -f null  -".format(
                self.__ffmpeg_path, self.__file_path, str(self.__ffmpeg_scene_change_threshold)
            )

        self.__scene_change_framenumbers = scene_change_frame_nmumbers

        self.__init_container_in()
        self.__start_frame      = start_frame
        self.__total_frames     = self.__duration_seconds*self.__fps
        self.__end_frame        = self.__total_frames if (end_frame==None) else min(self.__total_frames, end_frame)
        self.__frames_to_process = self.__end_frame - self.__start_frame
        assert (self.__end_frame > self.__start_frame)
        assert (self.__frames_to_process > 0)

        self.__frame_batches = self.__make_batches()


        self.__save_output   = save_output
        self.__output_path   = output_path
        self.__stream_out    = None
        self.__container_out = None

        if self.__output_path is None:
            head, tail = os.path.split(self.__file_path)
            model_name = os.path.splitext(os.path.split(self.dnn_classifier.classifier_path)[-1])[0]
            self.__output_path = f"{head}/out_{int(time.time())}_{model_name}_{tail[:-3]}mp4"

        self.__pix_fmt      = 'yuv420p'
        self.__codec_name   = 'h264'
        self.__frame_format = 'rgb24'

    def __init_container_in(self):
        self.__container_in     = av.open(self.__file_path, 'r')
        self.__stream_in        = self.__container_in.streams.video[0]
        self.__exact_fps        = self.__stream_in.average_rate.numerator/self.__stream_in.average_rate.denominator
        self.__fps              = round(self.__exact_fps, 2)
        self.__duration_seconds = self.__container_in.duration/1000000.0


    def __parse_ffmpeg_lines(self, output_string):

        frames = []

        lines = output_string.splitlines()
        for line in lines:
            if b'pts_time' in line:
                frame = line[46:57].decode("ascii").replace(" ", "").split(":")[1]
                frames.append(int(frame))
        return frames

    def __make_batches(self):

        assert(self.__batch_split_strategy in ['uniform', 'ffmpeg', "custom"])
        batch_sizes = []

        if self.__batch_split_strategy == 'uniform':

            # _______________________KEEEP______________________________
            # TODO Find a better fix or KEEP THIS, otherwise EOF exception.
            last_batch_size = int(self.__frames_to_process % self.__batch_size)
            full_batches = int(self.__frames_to_process / self.__batch_size)
            batch_sizes = [self.__batch_size for fb in range(full_batches)]
            if (last_batch_size == 1):
                batch_sizes[-1] += 1
            elif (last_batch_size != 0):
                batch_sizes.append(last_batch_size)

            if (self.__batch_size == 1):
                del batch_sizes[-2:]
                batch_sizes[-1] += 1
            # ___________________END_KEEEP_______________________________

        elif self.__batch_split_strategy == 'ffmpeg':
            # WARNING: this strategy is exerimental and may fail!
            assert(self.__ffmpeg_path is not None)
            #TODO make it work for a part of video.
            assert(self.__start_frame == 0)
            assert (self.__end_frame == self.__total_frames)
            process = Popen(self.__ffmpeg_cmd, stdout=PIPE, stderr=STDOUT)
            try:
                (stdout, stderr) = process.communicate(timeout=self.__ffmpeg_timeout)
                if (process.returncode == 0):
                    frames = self.__parse_ffmpeg_lines(stdout)
                    frames.insert(0, 0)
                    batch_sizes = [t - s for s, t in zip(frames, frames[1:])]

                    assert (len(batch_sizes)>0)

                    last_batch_size = batch_sizes[-1]
                    if (last_batch_size == 1):
                        batch_sizes[-1] += 1
                    elif (last_batch_size != 0):
                        batch_sizes.append(last_batch_size)

                else:
                    msg = "Exception durnin ffmpeg execution, CODE: {}, OUTPUT: {}".format(process.returncodem, stdout)
                    raise Exception(msg)
            except SubprocessError as se:
                print(str(se))

        elif self.__batch_split_strategy == 'custom':

            self.__scene_change_framenumbers.append(self.__end_frame)
            self.__scene_change_framenumbers.insert(0, self.__start_frame)
            batch_sizes = [t - s for s, t in zip(self.__scene_change_framenumbers, self.__scene_change_framenumbers[1:])]
            assert(0 not in batch_sizes)


        return batch_sizes

    def __skip_to_frame(self, frame_number):
        #Skip frames upto frame_number
        if (frame_number>0):
            for i, frame in enumerate(self.__container_in.decode(video=0)):
                if (i+1) == frame_number:
                    break

    def __init_output_file(self):
        self.__container_out = av.open(self.__output_path, mode='w')
        self.__stream_out = self.__container_out.add_stream(self.__codec_name, rate=self.__fps)
        self.__stream_out.width = self.__stream_in.width
        self.__stream_out.height = self.__stream_in.height
        self.__stream_out.pix_fmt = self.__pix_fmt

    def __write_to_output(self, frames, detections, classes, names, genders, confidences):
            # Draw results on frames
            for i in range(len(frames)):
                display_img = self.draw_annotations(frames[i],
                                                    detections=detections[i],
                                                    classes=classes[i],
                                                    names=names[i],
                                                    genders=genders[i],
                                                    confidences=confidences[i])
                frame = av.VideoFrame.from_ndarray(display_img, format=self.__frame_format)
                for packet in self.__stream_out.encode(frame):
                    self.__container_out.mux(packet)

    def __close_output_stream(self):
        # Flush stream
        for packet in self.__stream_out.encode():
            self.__container_out.mux(packet)
        self.__container_out.close()

    def __read_frame_batch(self, id):
        frame_batch = []
        # Collect frames for one batch to prcoess
        batch_size = self.__frame_batches[id]

        for i, frame in enumerate(self.__container_in.decode(video=0)):
            frame_batch.append(np.array(frame.to_image()))
            if (i + 1) == batch_size:
                break

        return frame_batch

    def __save_output_video(self):
        if self.__save_output:
            self.__init_output_file()

            self.__init_container_in()
            self.__skip_to_frame(self.__start_frame)

            for i_bs in range(len(self.__frame_batches)):

                print("Saving batch number {}/{}, frames in batch = {}".format(i_bs, len(self.__frame_batches),
                                                                                   self.__frame_batches[i_bs]))

                batch_size = self.__frame_batches[i_bs]
                batch_start_pos = int(np.sum(self.__frame_batches[:i_bs]))

                frame_batch       = self.__read_frame_batch(i_bs)
                detections_batch  = self.detections[batch_start_pos:batch_start_pos+batch_size]
                classes_batch     = self.classes[batch_start_pos:batch_start_pos+batch_size]
                names_batch       = self.names[batch_start_pos:batch_start_pos+batch_size]
                gender_batch      = self.genders[batch_start_pos:batch_start_pos+batch_size]
                confidences_batch = self.confidences[batch_start_pos:batch_start_pos + batch_size]

                self.__write_to_output(frames=frame_batch,
                                      detections=detections_batch,
                                      classes=classes_batch,
                                      names=names_batch,
                                      genders=gender_batch,
                                      confidences=confidences_batch)

            self.__close_output_stream()


    def process(self, save_output = True, save_output_path=None):
        """
        Function to apply recognition to a video file. This function is able to read a video part by part,
        which requires less RAM.

        :param save_output: If True, saves the resulting annotated video.
        :param save_output_path: Saves the output video at given location and name.
                                 If not set, the video is stored at same_location/out_<timestamp>_<origin_name>
        :return: json_result: json object containing recognition information ({'detections': detections, 'classes': classes, 'names': names}).
        """

        print("Start process video file: " + self.__file_path)

        detections = []; classes = []; confidences = []; names = []; genders = []; embs = [];
        frames_processed = 0

        self.__skip_to_frame(self.__start_frame)

        for i_bs in range(len(self.__frame_batches)):

            print("Processing batch number {}/{}, frames in batch = {}".format(i_bs, len(self.__frame_batches), self.__frame_batches[i_bs]))

            frame_batch = self.__read_frame_batch(i_bs)

            detections_batch, _, embs_batch, confidence_b, classes_batch, names_batch, gender_batch = self.process_images(frame_batch)

            detections.append(detections_batch)
            #crops.append(crops_batch)
            embs.append(embs_batch)
            classes.append(classes_batch)
            confidences.append(confidence_b)
            names.append(names_batch)
            genders.append(gender_batch)

            frames_processed += len(frame_batch)

        detections = [entry for sublist in detections for entry in sublist];
        classes = [entry for sublist in classes for entry in sublist]
        confidences = [entry for sublist in confidences for entry in sublist]
        names = [entry for sublist in names for entry in sublist]
        embs = [entry for sublist in embs for entry in sublist]
        genders = [entry for sublist in genders for entry in sublist]

        names, classes, confidences = self.cluster_ebmeddings(embs, names, classes, confidences)

        self.detections = detections
        self.classes = classes
        self.names = names
        self.genders = genders
        self.confidences = confidences

        self.__save_output_video()

        result = Recogniser.return_results(detections=detections, classes=classes, names=names, genders=genders, confidences=confidences)

        return result


    @staticmethod
    def test_video(args):
        """
        Test method for a video. See the bottom of the file for more details.

        :param args:
        """
        detector = MTCNNDetector(confidence_threshold = 0.98)
        embedder = Embedder(model_path = args.emb_model_path)
        dnn_classifier = DNNClassifier(classifier_path = args.dnn_classifier_model_path,
                                       class_map_path = args.dnn_classifier_id_name_map_path,
                                       threshold=args.dnn_classifier_conf_threshold)
        annoy_classifier = AnnoyKNNClassifier(annoy_index_file_path=args.annoy_index_file_path,
                                               id_name_map_path=args.annoy_id_name_map_path)
        #postprocessor = IoUPostprocessor(frame_window_size = 25, stride = 1, iou_th=0.6, min_detection_frequency=0.3)
        postprocessor = None

        recogniser = VideoRecogniser(args.input_file_path,
                                     detector=detector,
                                     embedder=embedder,
                                     dnn_classifier=dnn_classifier,
                                     annoy_classifier=annoy_classifier,
                                     postprocessor=postprocessor,
                                     start_frame=0, end_frame=None, batch_size=10, save_output=True)


        start = time.time()

        json_result = recogniser.process()

        end = time.time()
        total = end - start
        print("Total seconds: " + str(total))

        text_file = open("output.json", "w")
        jt.dump(json_result, text_file)
        text_file.close()
        #print(json)

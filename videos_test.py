if __name__ == "__main__":

    from  recogniser.rec_video import VideoRecogniser

    class AttrDict(dict):
        def __init__(self, *args, **kwargs):
            super(AttrDict, self).__init__(*args, **kwargs)
            self.__dict__ = self


    VIDEO_FILE_PATHS = [
        r"D:\ml\resources\videos\Merkel_Macron_EU.mp4",
        r"D:\ml\resources\videos\Trump_Hillary.mp4",
        r"D:\ml\resources\videos\dieter_bohlen.mp4",
        r"D:\ml\resources\videos\schwarznegger.mp4",
        r"D:\ml\resources\videos\angelina_jolie_and_brad_pitt.mp4",
        r"D:\ml\resources\videos\m_schumacher.mp4",
        r"D:\ml\resources\videos\helene_fischer.mp4",
        r"D:\ml\resources\videos\ukr_kriese.mp4",
        r"D:\ml\resources\videos\putin_merkel_2.mp4"
    ]

    args = {
        "input_file_path": "",
        "emb_model_path": r"D:\ml\resources\models\20180408-102900\20180408-102900.pb",
        "dnn_classifier_model_path": r"D:\ml\recognizer\resources\clean_higher_100_with_gender_v18_singlelabel_model_best.h5",
        "dnn_classifier_id_name_map_path": r"D:\ml\recognizer\resources\dnn_class_name_map_1629_persons.json",
        "dnn_classifier_conf_threshold": 0.6,
        "annoy_index_file_path": r"D:\ml\recognizer\resources\annoy_index_metric_euclidean_date_12082019_persons_1629_trees_30",
        "annoy_id_name_map_path": r"D:\ml\recognizer\resources\annoy_id_name_map.p",
    }


    for vfp in VIDEO_FILE_PATHS:
        from recogniser.rec_video import VideoRecogniser
        args['input_file_path'] = vfp
        args["dnn_classifier_model_path"] = r"D:\ml\recognizer\resources\clean_higher_100_with_gender_v18_singlelabel_model_best.h5"
        VideoRecogniser.test_video(AttrDict(args))
        args['dnn_classifier_model_path'] = r"D:\ml\recognizer\resources\temp.h5"
        VideoRecogniser.test_video(AttrDict(args))
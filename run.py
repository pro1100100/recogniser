import argparse
import os

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file_path', type=str, help='Test file path image or video.')
    parser.add_argument('emb_model_path', type=str, help='Path to facenet model.')
    parser.add_argument('dnn_classifier_model_path', type=str, help='Path to classifier model.')
    parser.add_argument('dnn_classifier_id_name_map_path', type=str, help='Path to classifier id -> name map.')
    parser.add_argument('--dnn_classifier_conf_threshold',
                        type=float, default=0.6,
                        help='Confidence threshold for person classification')
    parser.add_argument('--annoy_index_file_path', type=str, help='Path to Annoy index file.')
    parser.add_argument('--annoy_id_name_map_path', type=str, help='Path to annoy feature vector id - name map.')

    args = parser.parse_args()

    filename, file_extension = os.path.splitext(args.input_file_path)
    if file_extension in ['.png', '.jpg']:
        from recogniser.rec_image import ImageRecogniser
        ImageRecogniser.test_single_image(args)
    elif file_extension in ['.mp4', '.avi', '.mxf']: # TODO Add more extensions.
        from recogniser.rec_video import VideoRecogniser
        VideoRecogniser.test_video(args)
    elif file_extension in ['.txt']:
        from recogniser.rec_image import UrlImageRecogniser
        UrlImageRecogniser.test_on_images(args)
    else:
        raise Exception("File has not a valid extension e.g. .mp4, .avi, .jpg")
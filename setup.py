import os
import sys
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

def requirements(use_gpu=False):
    req =  [
        'MTCNN',
        'numpy==1.16.2',
        'opencv-python',
        'keras',
        'json_tricks',
        'requests',
        'Pillow',
        'annoy'
    ]
    if use_gpu:
        req.append('tensorflow-gpu')
    else:
        req.append('tensorflow')
    return req

use_gpu = True if  "--gpu" in sys.argv else False

setup(
    name = "mirecogniser",
    version = "0.0.5",
    author = "MI@CBC",
    author_email = "sergey.vandyhev@cbc-extern.de",
    description = ("Face recognition module."),
    license = "Ask CBC",
    packages=['recogniser'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities"
    ],
    install_requires=requirements(use_gpu),
    extras_require={
        'video_rec': ['av']
    }
)